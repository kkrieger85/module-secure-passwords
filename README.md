# Magento 2 Secure Passwords Module

While Magento 2 has improved its password hashing mechanism compared to its predecessor by using longer salts and switching over to SHA-256, it is still vulnerable to dictionary based brute-force attacks once an attacker compromises the database. You can read [this](http://gwillem.gitlab.io/2016/12/16/cracking-magento-passwords/) excellent blogpost by Willem de Groot that clarifies the problem. It is advised to use a slow hashing algorithm to block this attack vector.

This module adds support for PHPs native [password_hash](http://php.net/password_hash) and [password_verify](http://php.net/password_verify) functions to Magento 2, while keeping backwards compatibility with existing hashing mechanism.

# License #

This module is distributed under the following licenses:

* [OSL-3.0](https://opensource.org/licenses/OSL-3.0)

# About Cream #

Cream is an e-commerce solution provider for the Magento platform. You'll find an overview of all our (open source) projects [on our website](https://www.cream.nl/).