<?php
/**
 * Copyright © 2016 Cream. All rights reserved.
 * https://www.cream.nl/
 */
namespace Cream\SecurePasswords\Plugin;

use Magento\Framework\Encryption\Encryptor;

class AddPasswordHashPlugin
{
    /**
     * Get hash is not used internally by the Encryptor class, so we can safely override it here.
     * Do not override Encryptor::hash if you wish to preserve backwards-compatibility.
     *
     * @param Encryptor $subject
     * @param callable $proceed
     * @param array $args
     * @return string
     */
    public function aroundGetHash(Encryptor $subject, callable $proceed, ...$args)
    {
        return password_hash($args[0], PASSWORD_BCRYPT);
    }

    /**
     * Validates hash using password_verify if a BCRYPT hash is found, otherwise we fall back to
     * original Magento validation.
     *
     * @param Encryptor $subject
     * @param callable $proceed
     * @param array $args
     * @return bool
     */
    public function aroundIsValidHash(Encryptor $subject, callable $proceed, ...$args)
    {
        if (strpos($args[1], '$2y$') !== 0) {
            return $proceed(...$args);
        }
        return password_verify($args[0], $args[1]);
    }
}
